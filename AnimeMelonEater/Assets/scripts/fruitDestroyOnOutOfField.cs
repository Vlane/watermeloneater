﻿using UnityEngine;

public class fruitDestroyOnOutOfField : MonoBehaviour {


    [SerializeField]
    GameRegulation gr;

    private void OnTriggerEnter2D(Collider2D other)
    {
        soundManager.PlaySound("pop");
        if (other.tag == "pickup")
        {
            Destroy(other.gameObject);
            gr.DealNormalDamage();
        }
        if (other.tag == "powerup")
        {
            Destroy(other.gameObject);
        }
    }
}
