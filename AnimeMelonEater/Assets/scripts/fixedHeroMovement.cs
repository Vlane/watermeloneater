﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class fixedHeroMovement : MonoBehaviour
{
    public GameRegulation gr;
    private Rigidbody2D rb;
    bool isRight = true;
    public float acceleration;
    bool flipIsTouched;
    public bool canFlip = true;
    Vector2 movement;
    [SerializeField]
    float dir = 0.01f;
    [SerializeField]
    float controlOffset;
    float lastDir;
    public float maxTouchHeight;
    public float fifthOfCamera;



    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        fifthOfCamera = gr.camHalfedWorldWidth * 2 / 7;
    }


    void Update()
    {
        if (Input.touchCount > 0)
        {
            Touch lastTouch = Input.touches[Input.touches.Length - 1];
            if (lastTouch.phase == TouchPhase.Began)
            {
                if (lastTouch.position.y / Camera.main.pixelHeight < maxTouchHeight)
                {
                    lastDir = dir;

                    dir = Camera.main.ScreenToWorldPoint(lastTouch.position).x;
                    if (dir >= 0)
                        dir = 1;
                    else if (dir < 0)
                        dir = -1;

                    if (dir > 0 && lastDir < 0)
                    {
                        Flip();
                    }
                    else if (dir < 0 && lastDir > 0)
                    {
                        Flip();
                    }
                    rb.MovePosition(rb.position + new Vector2(fifthOfCamera * dir, 0));
                }
            }
        }


    }



    void Flip()
    {
        if (canFlip)
        {
            transform.localScale = new Vector2(transform.localScale.x * -1, transform.localScale.y);
            isRight = !isRight;
            // dir *= -1;
        }
    }

    IEnumerator DisableFlip()
    {
        canFlip = false;
        yield return new WaitForSeconds(0.2f);
        canFlip = true;
    }
}