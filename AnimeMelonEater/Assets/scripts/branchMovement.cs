﻿using System.Collections;
using UnityEngine;

public class branchMovement : MonoBehaviour
{
    private Rigidbody2D rb;
    public GameRegulation gr;
    short dir=1;
    // bool isRight = true;
    // public float acceleration;

    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        StartCoroutine("RndFlip");
    }


    void Update()
    {
        Vector2 movement = new Vector2(gr.playerSpeed * dir, 0);
        rb.MovePosition(rb.position + movement);
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        //if (other.tag == "wall")
        //{
        //    Flip();
        //    StartCoroutine("DisableFlip");
        //}
    }

    IEnumerator RndFlip()
    {
        while (true)
        {
            yield return new WaitForSeconds(Random.Range(gr.curObjGenMinRange/4,gr.curObjGenMinRange));
            Flip();
        }
    }

    void Flip()
    {
            transform.localScale = new Vector2(transform.localScale.x * -1, transform.localScale.y);
            dir *= -1;
    }

}
