﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Assets.Database;
using Mono.Data.Sqlite;
using System;
using System.Data;
using System.IO;
using UnityEngine.UI;

public class shopController : MonoBehaviour
{
    private int fruits;
    [SerializeField]
    private Text fruitText;
    private int premiumFruits;
    [SerializeField]
    private Text premiunFruitText;
    public int maxPULvl = 5;

    public enum IPUinList
    {
        trayTime,
        megaFruitMultiplyer,
        timeSlowTime,
        shieldPerPowerup,
        healMultiplyer,
        bombMultiplyer
    };

    public List<powerupShopItem> powerupShopItems = new List<powerupShopItem>();

    public int Fruits
    {
        get => fruits;
        set
        {
            fruits = value;
            fruitText.text = fruits.ToString();
        }
    }
    public int PremiumFruits
    {
        get => premiumFruits;
        set
        {
            premiumFruits = value;
            premiunFruitText.text = premiumFruits.ToString();
        }
    }

    private void SetPremCurInDB(int val)
    {
        using (SqliteConnection conn = DBHelper.ResDBconn)
        {
            conn.Open();

            using (SqliteCommand comm = conn.CreateCommand())
            {
                comm.CommandText = $"UPDATE {DBCnst.PremiumCur.Self} SET {DBCnst.PremiumCur.FruitAmount} = @fruits WHERE {DBCnst.PremiumCur.PlayerName} = @player_name;";
                comm.Parameters.Add("player_name", DbType.String).Value = "main_player";
                comm.Parameters.Add("fruits", DbType.Int32).Value = val;
                comm.ExecuteNonQuery();
            }
        }
    }

    private void SetCurInDB(int val)
    {
        using (SqliteConnection conn = DBHelper.ResDBconn)
        {
            conn.Open();

            using (SqliteCommand comm = conn.CreateCommand())
            {
                comm.CommandText = $"UPDATE {DBCnst.FruitCur.Self} SET {DBCnst.FruitCur.FruitAmount} = @fruits WHERE {DBCnst.FruitCur.PlayerName} = @player_name;";
                comm.Parameters.Add("player_name", DbType.String).Value = "main_player";
                comm.Parameters.Add("fruits", DbType.Int32).Value = val;
                comm.ExecuteNonQuery();
            }
        }
    }

    public void Awake()
    {
        using (SqliteConnection conn = DBHelper.ResDBconn)
        {
            conn.Open();

            using (SqliteCommand comm = conn.CreateCommand())
            {
                comm.CommandText = $"SELECT * FROM `{DBCnst.FruitCur.Self}` WHERE `{DBCnst.FruitCur.PlayerName}` = @player_name;";
                comm.Parameters.Add("player_name", DbType.String).Value = "main_player";
                SqliteDataReader reader = comm.ExecuteReader();

                reader.Read();

                Fruits = reader.GetInt32(DBCnst.FruitCur.FruitAmount);
                reader.Close();

                comm.CommandText = $"SELECT * FROM `{DBCnst.PremiumCur.Self}` WHERE `{DBCnst.PremiumCur.PlayerName}` = @player_name;";
                comm.Parameters.Add("player_name", DbType.String).Value = "main_player";
                reader = comm.ExecuteReader();

                reader.Read();

                PremiumFruits = reader.GetInt32(DBCnst.PremiumCur.FruitAmount);
                reader.Close();
            }
        }

        powerupShopItems[0].lvlDBColName = DBCnst.PlUpg.TrayTime;
        powerupShopItems[1].lvlDBColName = DBCnst.PlUpg.MegaFruitMultiplyer;
        powerupShopItems[2].lvlDBColName = DBCnst.PlUpg.TimeSlowTime;
        powerupShopItems[3].lvlDBColName = DBCnst.PlUpg.ShieldPerPowerup;
        powerupShopItems[4].lvlDBColName = DBCnst.PlUpg.HealMultiplyer;
        powerupShopItems[5].lvlDBColName = DBCnst.PlUpg.BombMultiplyer;

        using (SqliteConnection conn = DBHelper.PUDBconn)
        {
            conn.Open();

            using (SqliteCommand comm = conn.CreateCommand())
            {
                comm.CommandText = $"SELECT * FROM `{DBCnst.PlUpg.Self}` WHERE `{DBCnst.PlUpg.PlayerName}` = @player_name;";
                comm.Parameters.Add("player_name", DbType.String).Value = "main_player";
                SqliteDataReader reader = comm.ExecuteReader();

                reader.Read();

                int i = 1;
                foreach (powerupShopItem puItem in powerupShopItems)
                {
                    puItem.VarLvl = reader.GetInt32(i);
                    ++i;
                }
                reader.Close();


                foreach (powerupShopItem puItem in powerupShopItems)
                {
                    if (puItem.VarLvl < maxPULvl)
                    {
                        reader.Close();
                        comm.CommandText = $"SELECT * FROM `{DBCnst.PuUpgCost.Self}` WHERE `{DBCnst.PuUpgCost.NxtLvlLvl}` = @nxt_lvl";
                        comm.Parameters.Add("nxt_lvl", DbType.Int32).Value = puItem.VarLvl + 1;
                        reader = comm.ExecuteReader();
                        reader.Read();
                        puItem.NextLvlPrice = reader.GetInt32($"{DBCnst.PuUpgCost.UpgCost}");
                    }
                    else
                    {
                        puItem.SetUnupgradeable();
                    }
                }
                reader.Close();
            }
        }
    }

    public void UpgradeClicked(int orderInList)
    {
        soundManager.PlaySound("button");
        if (powerupShopItems[orderInList].NextLvlPrice <= Fruits && powerupShopItems[orderInList].VarLvl < maxPULvl)
        {
            Fruits -= powerupShopItems[orderInList].NextLvlPrice;

            SetCurInDB(Fruits);

            ++powerupShopItems[orderInList].VarLvl;
            using (SqliteConnection conn = DBHelper.PUDBconn)
            {
                conn.Open();

                using (SqliteCommand comm = conn.CreateCommand())
                {
                    comm.CommandText = $"UPDATE {DBCnst.PlUpg.Self} SET {powerupShopItems[orderInList].lvlDBColName} = @lvl WHERE {DBCnst.FruitCur.PlayerName} = @player_name;";
                    comm.Parameters.Add("player_name", DbType.String).Value = "main_player";
                    comm.Parameters.Add("lvl", DbType.Int32).Value = powerupShopItems[orderInList].VarLvl;
                    comm.ExecuteNonQuery();
                }
            }
            using (SqliteConnection conn = DBHelper.PUDBconn)
            {
                conn.Open();

                using (SqliteCommand comm = conn.CreateCommand())
                {
                    if (powerupShopItems[orderInList].VarLvl < maxPULvl)
                    {
                        comm.CommandText = $"SELECT * FROM `{DBCnst.PuUpgCost.Self}` WHERE `{DBCnst.PuUpgCost.NxtLvlLvl}` = @nxt_lvl";
                        comm.Parameters.Add("nxt_lvl", DbType.Int32).Value = powerupShopItems[orderInList].VarLvl + 1;
                        SqliteDataReader reader = comm.ExecuteReader();
                        reader.Read();
                        powerupShopItems[orderInList].NextLvlPrice = reader.GetInt32($"{DBCnst.PuUpgCost.UpgCost}");
                    }
                    else
                    {
                        powerupShopItems[orderInList].SetUnupgradeable();
                    }
                }
            }
        }
    }

    public void BuyPremCurClicked()
    {
        soundManager.PlaySound("button");
        PremiumFruits += 10;
        SetPremCurInDB(PremiumFruits);
    }

    public void BuyFruitsForPremClicked()
    {
        soundManager.PlaySound("button");
        if (PremiumFruits-10>=0)
        {
            PremiumFruits -= 10;
            SetPremCurInDB(PremiumFruits);
            Fruits += 1000;
            SetCurInDB(Fruits);
        }
    }

    //public int GetVarLvl(string colName)
    //{
    //    int temp;
    //    using (SqliteConnection conn = DBHelper.PUDBconn)
    //    {
    //        conn.Open();

    //        using (SqliteCommand comm = conn.CreateCommand())
    //        {
    //            comm.CommandText = $"SELECT * FROM `{DBCnst.PlUpg.Self}` WHERE `{DBCnst.PlUpg.PlayerName}` = @player_name;";
    //            comm.Parameters.Add("player_name", DbType.String).Value = "main_player";
    //            SqliteDataReader reader = comm.ExecuteReader();
    //            reader.Read();
    //            temp = reader.GetInt32(colName);
    //            reader.Close();
    //        }
    //    }
    //    return temp;
    //}


}
