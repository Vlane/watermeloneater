﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class powerupShopItem : MonoBehaviour
{
    public string lvlDBColName;
    private int varLvl;
    private int nextLvlPrice;
    [SerializeField]
    private Text varLvlTxt;
    [SerializeField]
    private Text nexLvlPriceTxt;

    #region props
    public int VarLvl
    {
        get => varLvl;
        set
        {
            varLvl = value;
            varLvlTxt.text = value.ToString();
        }
    }
    public int NextLvlPrice
    {
        get => nextLvlPrice;
        set
        {
            nextLvlPrice = value;
            nexLvlPriceTxt.text = value.ToString();
        }
    }
    #endregion

    public powerupShopItem(string LvlDBColName)
    {
        lvlDBColName = LvlDBColName;
    }

    public void SetUnupgradeable()
    {
        nexLvlPriceTxt.text = "MAX";
        nextLvlPrice = 999999;
    }
}
