﻿using System.Collections;
using UnityEngine;
using System;

public class simpleHeroMove : MonoBehaviour
{
    public GameRegulation gr;
    private Rigidbody2D rb;
    bool isRight = true;
    public float acceleration;
    bool flipIsTouched;
    public bool canFlip = true;
    Vector2 movement;
    [SerializeField]
    float dir = 0.01f;
    [SerializeField]
    float controlOffset;
    float lastDir;
    public float maxTouchHeight;




    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }


    void FixedUpdate()
    {
        //some outdated code
        {
            {
                //moveHorizontal = Input.GetAxisRaw("Horizontal");

                //if(moveHorizontal>0&&!isRight)
                //{
                //    flip();
                //}else if (moveHorizontal < 0 && isRight)
                //{
                //    flip();
                //}

                //Vector2 movement = new Vector2(moveHorizontal,0);

                //if(Input.GetKey(KeyCode.LeftShift) || Input.GetKey(KeyCode.RightShift))
                //{
                //    rb.MovePosition(rb.position + movement * speed * acceleration);
                //}else
                //{
                //    rb.MovePosition(rb.position + movement * speed);
                //}
            }
            {
                //    if (Input.touchCount > 0)
                //    {
                //        Touch[] touches = Input.touches;
                //        if (!flipIsTouched)
                //        {
                //            foreach (Touch touch in touches)
                //            {
                //                if (touch.position.x <= Camera.main.pixelWidth / 2)
                //                {
                //                    Flip();
                //                    flipIsTouched = true;
                //                    break;
                //                }
                //            }
                //        }
                //        else
                //        {
                //            flipIsTouched = false;
                //            foreach (Touch touch in touches)
                //            {
                //                if (touch.position.x <= Camera.main.pixelWidth / 2)
                //                {
                //                    flipIsTouched = true;
                //                }
                //            }
                //        }
                //    }
                //    else
                //    {
                //        if (flipIsTouched)
                //        {
                //            flipIsTouched = false;
                //        }
                //    }

                //    movement = new Vector2(gr.playerSpeed * dir, 0);
                //    if (Input.touchCount > 0)
                //    {
                //        Touch[] touches = Input.touches;
                //        foreach (Touch touch in touches)
                //        {
                //            if (touch.position.x > Camera.main.pixelWidth / 2)
                //            {
                //                movement = new Vector2(gr.playerSpeed * acceleration * dir, 0);
                //                break;
                //            }
                //        }
                //    }
            }
            {
                //    if (Input.touchCount > 0)
                //    {
                //        Touch lastTouch = Input.touches[Input.touches.Length - 1];
                //        if (isRight && lastTouch.position.x < Camera.main.pixelWidth / 2)
                //        {
                //            Flip();
                //        }
                //        else if (!isRight && lastTouch.position.x > Camera.main.pixelWidth / 2)
                //        {
                //            Flip();
                //        }
                //    }
            }
        }

        if (Input.touchCount > 0)
        {
            Touch lastTouch = Input.touches[Input.touches.Length - 1];
            if (lastTouch.position.y / Camera.main.pixelHeight < maxTouchHeight)
            {
                lastDir = dir;

                dir = Camera.main.ScreenToWorldPoint(lastTouch.position).x / (gr.camHalfedWorldWidth * (1 - controlOffset* 2));
                if (dir > 1)
                    dir = 1;
                else if (dir < -1)
                    dir = -1;

                if (dir >= 0 && lastDir < 0)
                {
                    Flip();
                }
                else if (dir < 0 && lastDir >= 0)
                {
                    Flip();
                }
                if (Math.Abs((rb.position + new Vector2(gr.playerSpeed * dir, 0)).x) <= gr.camHalfedWorldWidth)
                    rb.MovePosition(rb.position + new Vector2(gr.playerSpeed * dir*dir*dir, 0));
            }

        }


    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        //if (other.tag == "wall")
        //{
        //    Flip();
        //    StartCoroutine("DisableFlip");
        //}
    }

    void Flip()
    {
        if (canFlip)
        {
            transform.localScale = new Vector2(transform.localScale.x * -1, transform.localScale.y);
            isRight = !isRight;
            // dir *= -1;
        }
    }

    IEnumerator DisableFlip()
    {
        canFlip = false;
        yield return new WaitForSeconds(0.2f);
        canFlip = true;
    }
}
