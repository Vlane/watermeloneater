﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;


public class singleplayerMenuScript : MonoBehaviour
{
    
    [SerializeField]
    GameObject mainMenuPanel;
    [SerializeField]
    GameObject canvasSingleplayer;
    public Animator animator;


    void Awake()
    {
    }

    void Start()
    {
    }

    public void NormalSingleplayerButtonClicked()
    {
        soundManager.PlaySound("button");

        canvasSingleplayer.SetActive(true);
        animator.SetTrigger("mainToSingleplayer");
    }

    public void FixedSingleplayerButtonClicked()
    {
        soundManager.PlaySound("button");
        SceneManager.LoadScene("singleplayerFixedPositions");
    }

    public void BackButtonClicked()
    {
        soundManager.PlaySound("button");
        mainMenuPanel.SetActive(true);
        gameObject.SetActive(false);
    }
}
