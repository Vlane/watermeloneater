﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class animationCanvasesMainMenuEvents : MonoBehaviour
{
    public void OnMainToSingleplayerComplete()
    {
        SceneManager.LoadScene("singleplayer");
    }
}
