﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class mainMenuScript : MonoBehaviour
{
    
    [SerializeField]
    GameObject singlePlayerPanel;
    [SerializeField]
    GameObject settingsPanel;
    [SerializeField]
    GameObject canvasSingleplayer;



    void Start()
    {
        canvasSingleplayer.SetActive(false);
        singlePlayerPanel.SetActive(false);
        settingsPanel.SetActive(false);
    }

    public void SingleplayerButtonClicked()
    {
        soundManager.PlaySound("button");
        singlePlayerPanel.SetActive(true);
        gameObject.SetActive(false);
    }

    public void OpenShopClicked()
    {
        soundManager.PlaySound("button");
        SceneManager.LoadScene("shop");
    }

    public void SettingsClicked()
    {
        soundManager.PlaySound("button");
        settingsPanel.SetActive(true);
    }
}
