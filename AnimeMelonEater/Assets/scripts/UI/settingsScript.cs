﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;
using UnityEngine.UI;

public class settingsScript : MonoBehaviour
{
    [SerializeField]
    AudioMixer audioMixer;
    [SerializeField]
    Slider slider;

    public void Start()
    {
        float temp;
        audioMixer.GetFloat("mainVolume", out temp);
        slider.SetValueWithoutNotify(temp);
    }

    public void VolumeSliderChanged(float val)
    {
        audioMixer.SetFloat("mainVolume",val);
    }

    public void BackButtonClicked()
    {
        soundManager.PlaySound("button");
        gameObject.SetActive(false);
    }
}
