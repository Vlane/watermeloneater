﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class shopSetSymb : MonoBehaviour
{
    [SerializeField]
    Text backButton;
    public List<Text> puUpgButtonList;

    void Awake()
    {
        backButton.text = ((char)0xf100).ToString();
        foreach (Text button in puUpgButtonList)
        {
            button.text = ((char)0xf062).ToString();
        }
    }
}
