﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class mainMenuSymSet : MonoBehaviour
{
    [SerializeField]
    Text backButton1;
    [SerializeField]
    Text backButton2;
    [SerializeField]
    Text settingsButton;
    [SerializeField]
    Text SPpauseBtn;
    [SerializeField]
    GameObject canvasSingleplayer;

    void Awake()
    {
        backButton1.text = ((char)0xf100).ToString();
        backButton2.text = ((char)0xf100).ToString();
        settingsButton.text = ((char)0xf013).ToString();
        SPpauseBtn.text= ((char)0xf28b).ToString();
    }

    void Start()
    {
        canvasSingleplayer.SetActive(false);
    }
}
