﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class singlePlayerSetSymb : MonoBehaviour
{
    [SerializeField]
    Text pauseButton;
    [SerializeField]
    Text toMainMenu;
    [SerializeField]
    Text restartButton;
    [SerializeField]
    Text continueBtn;
    [SerializeField]
    GameObject pausePanel;
    [SerializeField]
    GameObject endPanel;

    void Awake()
    {
        pauseButton.text = ((char)0xf28b).ToString();
        toMainMenu.text = ((char)0xf100).ToString();
        restartButton.text = ((char)0xf0e2).ToString();
        continueBtn.text = ((char)0xf144).ToString();
    }

    void Start()
    {
        pausePanel.SetActive(false);
        endPanel.SetActive(false);
    }
}
