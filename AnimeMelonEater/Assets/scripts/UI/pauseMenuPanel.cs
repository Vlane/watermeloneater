﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class pauseMenuPanel : MonoBehaviour
{
    
    bool isPaused;
    [SerializeField]
    pauseButton pb;
  

    public void Pause()
    {
        gameObject.SetActive(true);
        Time.timeScale = 0f;
        isPaused = true;
    }

    public void Unpause()
    {
        gameObject.SetActive(false);
        Time.timeScale = 1f;
        isPaused = false;
        pb.gameObject.SetActive(true);
    }

    public void ToMainMenuClicked()
    {
        soundManager.PlaySound("button");
        Unpause();
        SceneManager.LoadScene("mainMenu");
    }

    public void RestartClicked()
    {
        soundManager.PlaySound("button");
        Unpause();
        SceneManager.LoadScene("singleplayer");
    }

    public void ContinueClicked()
    {
        soundManager.PlaySound("button");
        Unpause();
    }


}
