﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class pauseButton : MonoBehaviour
{
    [SerializeField]
    pauseMenuPanel pmp;

    void Start()
    {
        
    }


    public void PauseButtonClicked()
    {
        soundManager.PlaySound("button");
        gameObject.SetActive(false);
        pmp.Pause();
    }
}
