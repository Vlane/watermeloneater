﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class shopNonBuyUI : MonoBehaviour
{
    public GameObject skinsMenu;
    public GameObject powerupMenu;
    public GameObject buyMenu;

    public void Start()
    {
        skinsMenu.SetActive(false);
        powerupMenu.SetActive(true);
        buyMenu.SetActive(false);
    }

    public void SkinsMenuClicked()
    {
        soundManager.PlaySound("button");
        skinsMenu.SetActive(true);
        powerupMenu.SetActive(false);
        buyMenu.SetActive(false);
    }

    public void PowerupMenuClicked()
    {
        soundManager.PlaySound("button");
        skinsMenu.SetActive(false);
        powerupMenu.SetActive(true);
        buyMenu.SetActive(false);
    }

    public void BuyMenuClicked()
    {
        soundManager.PlaySound("button");
        skinsMenu.SetActive(false);
        powerupMenu.SetActive(false);
        buyMenu.SetActive(true);
    }

    public void ToMainMenuClicked()
    {
        soundManager.PlaySound("button");
        SceneManager.LoadScene("mainMenu");
    }


}
