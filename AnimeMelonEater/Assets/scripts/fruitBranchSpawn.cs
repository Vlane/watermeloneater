﻿using System.Collections;
using UnityEngine;

public class fruitBranchSpawn : MonoBehaviour {

    Rigidbody2D rb;
    public Transform fruit;

    void Start () {
        rb = GetComponent<Rigidbody2D>();
        StartCoroutine("SpawnFruits");
	}
	
    IEnumerator SpawnFruits()
    {
        while (true)
        {
            Instantiate(fruit, new Vector2(rb.transform.position.x, rb.transform.position.y), Quaternion.identity);
            yield return new WaitForSeconds(0.5f);
        }
    }

}
