﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class GameRegulation : MonoBehaviour
{
    public powerupHandler PuH;
    //OTM = over time multiplyer
    public float playerSpeed;
    public float objFallingSpeed;
    [SerializeField]
    public float initialObjGenMinRange;
    public float curObjGenMinRange;
    //public float playerSpeedOTM;
    //public float objFallingSpeedOTM;
    //public float objGenMinRangeOTM;
    public float multiplyerStep;
    public float multiplyerTime;
    public float objGenMinToMaxMultiplyer;
    public float HPperFruit;
    /// <summary>
    /// max fixed updates per fruit generatin
    /// </summary>
    float maxFUPF;
    /// <summary>
    /// min fixed updates per fruit generatin
    /// </summary>
    float minFUPF;
    [SerializeField]
    /// <summary>
    /// damage over time
    /// </summary>
    public float DOT;
    public float camHalfedWorldWidth;
    public int curIntMultiplyer;
    public int multiplyerDivider;
    [SerializeField]
    Text miltiplyText;
    [SerializeField]
    Text scoreText;
    [SerializeField]
    Text fruitsText;
    [SerializeField]
    int score = 0;
    public float scoreBaseAdjustment = 100;
    public float health = 1f;
    public ShieldBar shieldBar;
    [SerializeField]
    private int fruits;
    public bool isDead;

    public int Score 
    {
        get
        {
            return score; 
        }
        set
        {
            score = value;
            scoreText.text = score.ToString();
        }
    }
        
    public int Fruits
    {
        get => fruits;
        set 
        {
            fruits = value;
            fruitsText.text =  fruits.ToString();
        }
    }

    void Start()
    {
        camHalfedWorldWidth = Camera.main.ScreenToWorldPoint(new Vector2(Camera.main.pixelWidth, 0)).x ;
        isDead = false;
        Fruits = 0;   
        curIntMultiplyer =1 * multiplyerDivider;
        curObjGenMinRange = initialObjGenMinRange;
        Recalculate();
        RefreshMultiplyText();
        StartCoroutine("MultiplyDificulty");
        Score=0;
    }

    void Awake()
    {
        
    }

    public void Recalculate()
    {
        minFUPF = curObjGenMinRange / Time.fixedDeltaTime;
        maxFUPF = curObjGenMinRange * objGenMinToMaxMultiplyer / Time.fixedDeltaTime;
        CalculateDOT();
        CalculatePlayerSpeed();
    }

    public void CalculateDOT()
    {
        DOT = HPperFruit / maxFUPF;
    }

    public void CalculatePlayerSpeed()
    {
        playerSpeed = 2f*camHalfedWorldWidth / minFUPF;
    }

    public void ChangeMultiplyer(float changeValue)
    {
        curIntMultiplyer +=(int)(changeValue*multiplyerDivider);
        RefreshMultiplyText();
        curObjGenMinRange = initialObjGenMinRange /(GetMultiplyer());
        Recalculate();
    }

    public float GetMultiplyer()
    {
        return (float)curIntMultiplyer / multiplyerDivider;
    }

    public void RefreshMultiplyText()
    {
        miltiplyText.text = $"x{GetMultiplyer()}";
    }

    IEnumerator MultiplyDificulty()
    {
        while (true)
        {
            yield return new WaitForSeconds(multiplyerTime);
            ChangeMultiplyer(multiplyerStep);
        }
    }

    public void DealNormalDamage()
    {
        if (shieldBar.shieldsCount > 0)
        {
            shieldBar.RemoveShield();
        }
        else
        {
            health -= HPperFruit;
        }
    }

    public void DealMultiplyedDamage(int damageMultiplyer)
    {
        health -= shieldBar.RemoveShields(damageMultiplyer)*HPperFruit;
    }
}
