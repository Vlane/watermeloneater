﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class soundManager : MonoBehaviour
{
    public static AudioClip buttonSnd, powerupSnd, bombSnd, eatSnd, popSnd;
    static AudioSource audioSource;

    void Start()
    {
        buttonSnd = Resources.Load<AudioClip>("buttonClickSnd");
        powerupSnd = Resources.Load<AudioClip>("PUpickupSnd");
        bombSnd = Resources.Load<AudioClip>("bombSnd");
        eatSnd = Resources.Load<AudioClip>("eatSnd");
        popSnd = Resources.Load<AudioClip>("popSnd");

        audioSource = GetComponent<AudioSource>();
    }

    public static void PlaySound(string clip)
    {
        switch (clip)
        {
            case "button":
                audioSource.PlayOneShot(buttonSnd);
                break;
            case "bomb":
                audioSource.PlayOneShot(bombSnd);
                break;
            case "powerup":
                audioSource.PlayOneShot(powerupSnd);
                break;
            case "eat":
                audioSource.PlayOneShot(eatSnd);
                break;
            case "pop":
                audioSource.PlayOneShot(popSnd);
                break;
        }
    }
}
