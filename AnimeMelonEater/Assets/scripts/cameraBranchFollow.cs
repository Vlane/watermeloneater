﻿using UnityEngine;

public class cameraBranchFollow : MonoBehaviour
{
    [SerializeField]
    bool cameraOnPlace=true;
    public Transform branch;
    public float defSmoothSpeed=0.01f;
    public float smoothSpeed = 0.011f;
    public float maxOffset = 0.2f;
    public float minOffset = 0.01f;
     public float smoothSpeedMultiplyer = 1.1f;


    void FixedUpdate()
    {
        if(cameraOnPlace&&(Camera.main.WorldToScreenPoint(branch.position).x>=Camera.main.pixelWidth*(1-maxOffset)|| Camera.main.WorldToScreenPoint(branch.position).x <= Camera.main.pixelWidth * maxOffset))
        {
            cameraOnPlace = false;
            smoothSpeed = defSmoothSpeed;
        }
        if(!cameraOnPlace)
        {
            transform.position =new Vector3(transform.position.x+(branch.position.x- transform.position.x)*smoothSpeed*smoothSpeed,transform.position.y,transform.position.z);
            smoothSpeed *= smoothSpeedMultiplyer;
            if (Camera.main.WorldToScreenPoint(branch.position).x-Camera.main.WorldToScreenPoint(transform.position).x <= minOffset * Camera.main.pixelWidth && Camera.main.WorldToScreenPoint(branch.position).x - Camera.main.WorldToScreenPoint(transform.position).x >= minOffset* -Camera.main.pixelWidth)
            {
                cameraOnPlace = true;
            }
        }
    }
}
