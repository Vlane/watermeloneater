﻿using System.Collections;
using System.Collections.Generic;
using System.Xml;
using System.Xml.Serialization;
using UnityEngine;
using Mono.Data.Sqlite;
using System;
using System.Data;
using System.IO;
using UnityEngine.UI;
using Assets.Database;


public class powerupHandler : MonoBehaviour
{

    public float normalToPUGenRate = 1f;
    public List<PowerUP> puList;
    public Tray tray;
    public float timeSlowTimeScale;
    float slowTimeRemaining = 0f;



    public enum IPUinList
    {
        trayTime,
        megaFruitMultiplyer,
        timeSlowTime,
        shieldPerPowerup,
        healMultiplyer,
        bombMultiplyer
    };
    public List<DBPUVar> DBPUVars = new List<DBPUVar>();


    public void Awake()
    {
        DBPUVars.Add(new DBPUVar(DBCnst.PlUpg.TrayTime, DBCnst.PULvlVal.TrayTime));
        DBPUVars.Add(new DBPUVar(DBCnst.PlUpg.MegaFruitMultiplyer, DBCnst.PULvlVal.MegaFruitMultiplyer));
        DBPUVars.Add(new DBPUVar(DBCnst.PlUpg.TimeSlowTime, DBCnst.PULvlVal.TimeSlowTime));
        DBPUVars.Add(new DBPUVar(DBCnst.PlUpg.ShieldPerPowerup, DBCnst.PULvlVal.ShieldPerPowerup));
        DBPUVars.Add(new DBPUVar(DBCnst.PlUpg.HealMultiplyer, DBCnst.PULvlVal.HealMultiplyer));
        DBPUVars.Add(new DBPUVar(DBCnst.PlUpg.BombMultiplyer, DBCnst.PULvlVal.BombMultiplyer));



        using (SqliteConnection conn = DBHelper.PUDBconn)
        {
            conn.Open();

            using (SqliteCommand comm = conn.CreateCommand())
            {
                comm.CommandText = $"SELECT * FROM `{DBCnst.PlUpg.Self}` WHERE `{DBCnst.PlUpg.PlayerName}` = @player_name;";
                comm.Parameters.Add("player_name", DbType.String).Value = "main_player";
                SqliteDataReader reader = comm.ExecuteReader();


                reader.Read();

                int i = 1;
                foreach (DBPUVar dBPUVar in DBPUVars)
                {
                    dBPUVar.varLvl = reader.GetInt32(i);
                    ++i;
                }

                
                foreach (DBPUVar dBPUVar in DBPUVars)
                {
                    reader.Close();
                    comm.CommandText = $"SELECT * FROM `{DBCnst.PULvlVal.Self}` WHERE `{DBCnst.PULvlVal.Level}` = @cur_lvl";
                    comm.Parameters.Add("cur_lvl", DbType.Int32).Value = dBPUVar.varLvl;
                    reader = comm.ExecuteReader();
                    reader.Read();
                    dBPUVar.varVal = reader.GetFieldValue<object>($"{dBPUVar.puLvlsValName}");
                }


                reader.Close();
            }
        }
    }


    public void Start()
    {

    }

    public void RestartSlowTime()
    {
        StopCoroutine("SlowTime");
        StartCoroutine("SlowTime");
    }

    IEnumerator SlowTime()
    {
        Time.timeScale = timeSlowTimeScale;   //TimeSlowScaleToValue ;
        yield return new WaitForSeconds((float)DBPUVars[(int)IPUinList.timeSlowTime].varVal);
        Time.timeScale = 1f;
    }
}
