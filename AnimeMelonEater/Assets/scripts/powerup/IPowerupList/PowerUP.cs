﻿using UnityEngine;


public abstract class PowerUP:MonoBehaviour
{
    public abstract void Activate(GameRegulation gr);
}
