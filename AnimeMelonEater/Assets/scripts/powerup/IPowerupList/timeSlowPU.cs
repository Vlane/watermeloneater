﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class timeSlowPU : PowerUP
{
    public override void Activate(GameRegulation gr)
    {
        soundManager.PlaySound("powerup");
        gr.PuH.RestartSlowTime();
    }
}
