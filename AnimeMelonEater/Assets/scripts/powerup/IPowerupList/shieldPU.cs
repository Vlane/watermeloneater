﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class shieldPU : PowerUP
{
    public override void Activate(GameRegulation gr)
    {
        soundManager.PlaySound("powerup");
        gr.shieldBar.AddShields(Convert.ToInt32(gr.PuH.DBPUVars[(int)powerupHandler.IPUinList.shieldPerPowerup].varVal));
    }
}
