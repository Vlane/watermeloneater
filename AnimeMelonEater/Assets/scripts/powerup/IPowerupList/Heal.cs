﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class Heal : PowerUP
{
    public override void Activate(GameRegulation gr)
    {
        soundManager.PlaySound("powerup");
        gr.health += gr.HPperFruit * Convert.ToInt32(gr.PuH.DBPUVars[(int)powerupHandler.IPUinList.healMultiplyer].varVal);
    }
}
