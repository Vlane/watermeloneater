﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrayPU : PowerUP
{
    public override void Activate(GameRegulation gr)
    {
        soundManager.PlaySound("powerup");
        gr.PuH.tray.SetTimer((float)gr.PuH.DBPUVars[(int)powerupHandler.IPUinList.trayTime].varVal);
    }
}
