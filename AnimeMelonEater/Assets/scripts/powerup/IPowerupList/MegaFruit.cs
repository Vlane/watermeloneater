﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class MegaFruit : PowerUP
{
    public override void Activate(GameRegulation gr)
    {
        soundManager.PlaySound("powerup");
        gr.Fruits += Convert.ToInt32(gr.PuH.DBPUVars[(int)powerupHandler.IPUinList.megaFruitMultiplyer].varVal);
        gr.Score +=(int)( gr.scoreBaseAdjustment * (float)gr.PuH.DBPUVars[(int)powerupHandler.IPUinList.megaFruitMultiplyer].varVal * gr.GetMultiplyer());
        gr.health += gr.HPperFruit;
    }
}
