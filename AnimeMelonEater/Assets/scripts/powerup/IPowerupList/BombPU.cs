﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class BombPU : PowerUP
{
    public override void Activate(GameRegulation gr)
    {
        soundManager.PlaySound("bomb");
        gr.DealMultiplyedDamage(Convert.ToInt32(gr.PuH.DBPUVars[(int)powerupHandler.IPUinList.bombMultiplyer].varVal));
    }
}
