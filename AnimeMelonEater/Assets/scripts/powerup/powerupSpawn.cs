﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class powerupSpawn : MonoBehaviour
{

    powerupHandler PuH;
    [SerializeField]
    GameRegulation gr;

    void Start()
    {
        StartSpawnig();
    }

    void StartSpawnig()
    {
        PuH = gr.PuH;
        StartCoroutine("SpawnPowerups");
        StartCoroutine("SpawnBombs");
    }

    void StopSpawning()
    {
        StopCoroutine("SpawnPowerups");
        StopCoroutine("SpawnBombs");
    }


    IEnumerator SpawnPowerups()
    {
        while (true)
        {
            yield return new WaitForSeconds(Random.Range(gr.initialObjGenMinRange, gr.initialObjGenMinRange * gr.objGenMinToMaxMultiplyer) * PuH.normalToPUGenRate);
            Instantiate(PuH.puList[Random.Range(0, PuH.puList.Count - 1)], Camera.main.ScreenToWorldPoint(new Vector2(Random.Range(0, Camera.main.pixelWidth), Camera.main.pixelHeight * 1.1f), 0), Quaternion.identity);
        }
    }

    IEnumerator SpawnBombs()
    {
        while (true)
        {

            yield return new WaitForSeconds(Random.Range(gr.initialObjGenMinRange / 2f, gr.initialObjGenMinRange * gr.objGenMinToMaxMultiplyer) * PuH.normalToPUGenRate * 2f);
            Instantiate(PuH.puList[PuH.puList.Count - 1], Camera.main.ScreenToWorldPoint(new Vector2(Random.Range(0, Camera.main.pixelWidth), Camera.main.pixelHeight * 1.1f), 0), Quaternion.identity);
        }
    }
}
