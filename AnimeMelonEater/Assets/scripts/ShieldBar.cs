﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ShieldBar : MonoBehaviour
{
    public GameObject[] shieldsArr = new GameObject[5];//maxShields = .Length
    public int shieldsCount = 0;
    [SerializeField]
    GameObject shieldVisualization;


    void Start()
    {
        foreach (GameObject shield in shieldsArr)
        {
            shield.SetActive(false);
        }
        shieldVisualization.SetActive(false);
    }

    public void AddShields(int shieldsAddCount)
    {
        for (int i = 0; i < shieldsAddCount; i++)
        {
            if (shieldsCount < shieldsArr.Length)
            {
                shieldsArr[shieldsCount].SetActive(true);
                shieldsCount++;
            }
            else
            {
                break;
            }
        }
        if (shieldsCount > 0)
            shieldVisualization.SetActive(true);
    }

    public void AddShield()
    {
        if (shieldsCount < shieldsArr.Length)
        {
            shieldsArr[shieldsCount].SetActive(true);
            shieldsCount++;
            if (shieldsCount > 0)
                shieldVisualization.SetActive(true);
        }
    }

    public void RemoveShield()
    {
        if (shieldsCount > 0)
            shieldsCount--;
        shieldsArr[shieldsCount].SetActive(false);
        if (shieldsCount <= 0)
            shieldVisualization.SetActive(false);
    }

    public int RemoveShields(int shieldsRemoveCount)
    {
        if (shieldsRemoveCount > 0)
        {
            int returnVal;
            if (shieldsRemoveCount > shieldsCount)
            {
                returnVal = shieldsRemoveCount - shieldsCount;
                while (shieldsCount > 0)
                {
                    shieldsCount--;
                    shieldsArr[shieldsCount].SetActive(false);
                }
            }
            else
            {
                returnVal = 0;
                int temp = shieldsCount - shieldsRemoveCount;
                for (int i = shieldsCount; i > temp; --i)
                {
                    shieldsCount--;
                    shieldsArr[shieldsCount].SetActive(false);
                }
            }
            if (shieldsCount <= 0)
                shieldVisualization.SetActive(false);
            return returnVal;
        }
        else
        {
            return 0;
        }
    }
}
