﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Assets.Database;
using Mono.Data.Sqlite;
using System;
using System.Data;
using System.IO;
using UnityEngine.UI;
using Assets.Database;

public class HealthBar : MonoBehaviour
{

    [SerializeField]
    GameRegulation gr;
    [SerializeField]
    DeathController ENDOFGAME;
    void Start()
    {
        gr.health = 1f;
    }

    void FixedUpdate()
    {
        DealDOT();
    }

    void DealDOT()
    {
        gr.health -= gr.DOT;
    }

    void LateUpdate()
    {
        UpdateHealthAndHB();
        if (gr.health <= 0f&&!gr.isDead)
        {
            gr.isDead = true;
            { 
            //int tempFruit = 0;
            //using (SqliteConnection conn = DBHelper.ResDBconn)
            //{
            //    conn.Open();

            //    using (SqliteCommand comm = conn.CreateCommand())
            //    {
            //        comm.CommandText = $"SELECT * FROM `{DBCnst.FruitCur.Self}` WHERE `{DBCnst.PlUpg.PlayerName}` = @player_name;";
            //        comm.Parameters.Add("player_name", DbType.String).Value = "main_player";
            //        SqliteDataReader reader = comm.ExecuteReader();

            //        reader.Read();

            //        tempFruit = reader.GetInt32(DBCnst.FruitCur.FruitAmount);
            //        reader.Close();
            //        tempFruit += gr.Fruits;

            //        comm.CommandText = $"UPDATE {DBCnst.FruitCur.Self} SET {DBCnst.FruitCur.FruitAmount} = @fruits WHERE {DBCnst.FruitCur.PlayerName} = @player_name;";
            //        comm.Parameters.Add("player_name", DbType.String).Value = "main_player";
            //        comm.Parameters.Add("fruits", DbType.Int32).Value = tempFruit;
            //        comm.ExecuteNonQuery();
            //    }
            //}
        }//commented
            ENDOFGAME.PreDeath();
        }
    }

    void UpdateHealthAndHB()
    {
        if (gr.health > 1)
            gr.health = 1;
        if (gr.health < 0)
            gr.health = 0;

        transform.localScale = new Vector3(gr.health, 1, 1);
    }
}
