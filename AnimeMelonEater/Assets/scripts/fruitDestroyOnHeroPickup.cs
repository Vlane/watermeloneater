﻿using UnityEngine;
using UnityEngine.UI;

public class fruitDestroyOnHeroPickup : MonoBehaviour {


   
    [SerializeField]
    GameRegulation gr;



    private void OnTriggerEnter2D(Collider2D other)
    { 
        if (other.tag == "pickup")
        {
            soundManager.PlaySound("eat");
            gr.health += gr.HPperFruit;
            Destroy(other.gameObject);
            gr.Score +=  (int)(gr.scoreBaseAdjustment * gr.GetMultiplyer()) ;
            ++gr.Fruits;
        }
        if (other.tag == "powerup")
        {
            other.GetComponent<PowerUP>().Activate(gr);
            Destroy(other.gameObject);
        }
    }
}
