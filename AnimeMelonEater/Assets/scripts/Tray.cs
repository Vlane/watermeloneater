﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tray : MonoBehaviour
{
    float timeRemaining=0f;
    [SerializeField]
    GameObject tray;

    void Start()
    {
        tray.SetActive(false);
    }

    public void SetTimer(float timeSet)
    {
        timeRemaining = timeSet;
        if(!tray.activeSelf)
        {
            tray.SetActive(true);
        }
    }

    void FixedUpdate()
    {
        if(tray.activeSelf)
        {
            timeRemaining -= Time.fixedDeltaTime;
            if(timeRemaining<=0)
            {
                tray.SetActive(false);
            }
        }
    }
}
