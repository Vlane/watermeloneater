﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class crossesspawn : MonoBehaviour {

    
    public Transform brick;
    public float ranWidth = 7;
    public float dropHeight=11;
    void Start () {
        StartCoroutine("SpawnCrosses");
	}
	
    IEnumerator SpawnCrosses()
    {
        while (true)
        {
            float ran = Random.value * ranWidth * 2;
            Instantiate(brick, new Vector2(ran - ranWidth, dropHeight), Quaternion.identity);
            yield return new WaitForSeconds(0.5f);
        }
    }


}
