﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Assets.Database;
using Mono.Data.Sqlite;
using System;
using System.Data;
using System.IO;
using UnityEngine.UI;
using Assets.Database;

public class DeathController : MonoBehaviour
{
    [SerializeField]
    GameRegulation gr;
    [SerializeField]
    Text finalScoreTxt;
    [SerializeField]
    Text fruitTxt;
    [SerializeField]
    GameObject endScreenPanel;
    int highscore;
    [SerializeField]
    Text highscoreTxt;
    [SerializeField]
    Text btnReviveTxt;
    int reviveCost = 0;
    int premFruits;


    public int GetPremCurFromDB()
    {
        int retVal;
        using (SqliteConnection conn = DBHelper.ResDBconn)
        {
            conn.Open();

            using (SqliteCommand comm = conn.CreateCommand())
            {
                comm.CommandText = $"SELECT * FROM `{DBCnst.PremiumCur.Self}` WHERE `{DBCnst.PremiumCur.PlayerName}` = @player_name;";
                comm.Parameters.Add("player_name", DbType.String).Value = "main_player";//main_player
                SqliteDataReader reader = comm.ExecuteReader();

                reader.Read();

                retVal = reader.GetInt32(DBCnst.PremiumCur.FruitAmount);
            }
        }
        return retVal;
    }

    public int GetHighScoreFromDB()
    {
        int retVal;
        using (SqliteConnection conn = DBHelper.RecordsDBconn)
        {
            conn.Open();

            using (SqliteCommand comm = conn.CreateCommand())
            {
                comm.CommandText = $"SELECT * FROM `{DBCnst.BestScores.Self}` WHERE `{DBCnst.BestScores.PlayerName}` = @player_name;";
                comm.Parameters.Add("player_name", DbType.String).Value = "main_player";//main_player
                SqliteDataReader reader = comm.ExecuteReader();

                reader.Read();

                retVal = reader.GetInt32(DBCnst.BestScores.BestScore);
            }
        }
        return retVal;
    }

    public void UpdateHighScoreInDB(int newHighscore)
    {
        using (SqliteConnection conn = DBHelper.RecordsDBconn)
        {
            conn.Open();

            using (SqliteCommand comm = conn.CreateCommand())
            {
                comm.CommandText = $"UPDATE {DBCnst.BestScores.Self} SET {DBCnst.BestScores.BestScore} = @score WHERE {DBCnst.BestScores.PlayerName} = @player_name;";
                comm.Parameters.Add("player_name", DbType.String).Value = "main_player";
                comm.Parameters.Add("score", DbType.Int32).Value = newHighscore;
                comm.ExecuteNonQuery();
            }
        }
    }

    public void UpdatePremCurInDB(int newFruit)
    {
        using (SqliteConnection conn = DBHelper.ResDBconn)
        {
            conn.Open();

            using (SqliteCommand comm = conn.CreateCommand())
            {
                comm.CommandText = $"UPDATE {DBCnst.PremiumCur.Self} SET {DBCnst.PremiumCur.FruitAmount} = @fruits WHERE {DBCnst.PremiumCur.PlayerName} = @player_name;";
                comm.Parameters.Add("player_name", DbType.String).Value = "main_player";
                comm.Parameters.Add("fruits", DbType.Int32).Value = newFruit;
                comm.ExecuteNonQuery();
            }
        }
    }

    public void Start()
    {
        endScreenPanel.SetActive(false);
    }

    public void PreDeath()
    {
        reviveCost = reviveCost * 2 + 1;
        Time.timeScale = 0f;
        btnReviveTxt.text = ((char)0xf004).ToString()+": " + reviveCost;
        finalScoreTxt.text = "score: "+"\n"+gr.Score;
        fruitTxt.text = "fruit:" + "\n" + gr.Fruits;
        highscore = GetHighScoreFromDB();
        if (highscore >= gr.Score)
        {
            highscoreTxt.text = "best score:\n" + highscore;
        }
        else
        {
            UpdateHighScoreInDB(gr.Score);
            highscoreTxt.text = "NEW BEST";
        }
        endScreenPanel.SetActive(true);
    }

    public void ConfirmDeath()
    {
        int tempFruit = 0;
        using (SqliteConnection conn = DBHelper.ResDBconn)
        {
            conn.Open();

            using (SqliteCommand comm = conn.CreateCommand())
            {
                comm.CommandText = $"SELECT * FROM `{DBCnst.FruitCur.Self}` WHERE `{DBCnst.PlUpg.PlayerName}` = @player_name;";
                comm.Parameters.Add("player_name", DbType.String).Value = "main_player";
                SqliteDataReader reader = comm.ExecuteReader();

                reader.Read();

                tempFruit = reader.GetInt32(DBCnst.FruitCur.FruitAmount);
                reader.Close();
                tempFruit += gr.Fruits;

                comm.CommandText = $"UPDATE {DBCnst.FruitCur.Self} SET {DBCnst.FruitCur.FruitAmount} = @fruits WHERE {DBCnst.FruitCur.PlayerName} = @player_name;";
                comm.Parameters.Add("player_name", DbType.String).Value = "main_player";
                comm.Parameters.Add("fruits", DbType.Int32).Value = tempFruit;
                comm.ExecuteNonQuery();
            }
        }
    }

    public void ReviveButtonClicked()
    {
        soundManager.PlaySound("button");
        premFruits = GetPremCurFromDB();
        if(premFruits>=reviveCost)
        {
            UpdatePremCurInDB(premFruits - reviveCost);
            gr.health = 1f;
            gr.isDead = false;
            endScreenPanel.SetActive(false);
            StartCoroutine("WaitToRevive");
        }
        else
        {

        }
    }

    IEnumerator WaitToRevive()
    {
        yield return new WaitForSecondsRealtime(2f);
        Time.timeScale = 1f;
    }


}
