﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class fruitFall : MonoBehaviour {

    public Rigidbody2D rb;
    public float speed;

	// Use this for initialization
	void Start () {
        rb = GetComponent<Rigidbody2D>();
	}
	
	// Update is called once per frame
	void FixedUpdate () {
        Vector2 movement = new Vector2(0, -1*speed);
        rb.MovePosition(rb.position+movement);

	}
}
