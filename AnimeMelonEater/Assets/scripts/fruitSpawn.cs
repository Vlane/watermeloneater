﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class fruitSpawn : MonoBehaviour
{
    public Transform fruit;
    [SerializeField]
    GameRegulation gr;

    void Start()
    {
        //Instantiate(fruit, new Vector3(gr.camWorldWidth,10 ,0), Quaternion.identity);
        StartCoroutine("SpawnFruits");
    }

    IEnumerator SpawnFruits()
    {
        while (true)
        {
           // Instantiate(fruit, new Vector3(gr.camHalfedWorldWidth, 10, 0), Quaternion.identity);
            yield return new WaitForSeconds(Random.Range(gr.curObjGenMinRange, gr.curObjGenMinRange * gr.objGenMinToMaxMultiplyer));
            Instantiate(fruit, Camera.main.ScreenToWorldPoint(new Vector2(Random.Range(0, Camera.main.pixelWidth), Camera.main.pixelHeight * 1.1f), 0), Quaternion.identity);
        }

        //while (true)
        //{
        //    Instantiate(fruit, Camera.main.ScreenToWorldPoint(new Vector2(Camera.main.pixelWidth, Camera.main.pixelHeight * 1.1f), 0), Quaternion.identity);
        //    Instantiate(fruit, Camera.main.ScreenToWorldPoint(new Vector2(0, Camera.main.pixelHeight * 1.1f), 0), Quaternion.identity);
        //    yield return new WaitForSeconds(gr.objGenMinRange);
        //}
    }
}
