﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AdaptRez : MonoBehaviour
{
    [SerializeField]
    private Transform barTrans;
    [SerializeField]
    private Transform playerTrans;
    [SerializeField]
    private float _size = 18.5f; // default size.

    private void Awake()
    {
        int camPixWidth = Camera.main.pixelWidth;
        Camera.main.orthographicSize = (_size * Screen.height / 2) / Screen.width;
        barTrans.position = Camera.main.ScreenToWorldPoint(new Vector3(camPixWidth/2,0,10))+new Vector3(0,0.1f);
        playerTrans.position = Camera.main.ScreenToWorldPoint(new Vector3(camPixWidth / 2, 0, 10)) + new Vector3(0, 1.75f);
    }
}
