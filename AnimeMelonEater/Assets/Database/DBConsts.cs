﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Database
{
    static class DBCnst
    {
        public static class FruitCur
        {
            public static string Self = "fruit_currencies";
            public static string PlayerName = "player_name";
            public static string FruitAmount = "fruits_amount";
        }

        public static class PremiumCur
        {
            public static string Self = "premium_fruit_currencies";
            public static string PlayerName = "player_name";
            public static string FruitAmount = "fruits_amount";
        }

        public static class PlUpg
        {
            public static string Self = "players_upgrades";
            public static string PlayerName = "player_name";
            public static string TrayTime = "tray_time";
            public static string MegaFruitMultiplyer = "mega_fruit_multiplyer";
            public static string TimeSlowTime = "time_slow_time";
            public static string ShieldPerPowerup = "shield_per_powerup";
            public static string HealMultiplyer = "heal_multiplyer";
            public static string BombMultiplyer = "bomb_multiplyer";
        }

        public static class PULvlVal
        {
            public static string Self = "pu_levels_values";
            public static string Level = "level";
            public static string TrayTime = "tray_time";
            public static string MegaFruitMultiplyer = "mega_fruit_multiplyer";
            public static string TimeSlowTime = "time_slow_time";
            public static string ShieldPerPowerup = "shield_per_powerup";
            public static string HealMultiplyer = "heal_multiplyer";
            public static string BombMultiplyer = "bomb_multiplyer";
        }

        public static class PuUpgCost
        {
            public static string Self = "pu_upg_cost";
            public static string NxtLvlLvl = "nxt_lvl_lvl";
            public static string UpgCost = "upg_cost";
        }

        public static class BestScores
        {
            public static string Self = "best_scores";
            public static string PlayerName = "player_name";
            public static string BestScore = "best_score";
        }
    }
}
