﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Mono.Data.Sqlite;
using System.Data;
using System.IO;
using UnityEngine.UI;
using UnityEngine;


namespace Assets.Database
{
    class DBHelper
    {
        private static string pUDatabaseName = "powerUP.s3db";
        static SqliteConnection pUDBConn;
        private static string pUDBPath = Application.persistentDataPath + "/" + pUDatabaseName;
        static string pUDBConnectinString = $"Data source={pUDBPath}; Version=3";

        private static string resDatabaseName = "playersResources.s3db";
        static SqliteConnection resDBConn;
        private static string resBPath = Application.persistentDataPath + "/" + resDatabaseName;
        static string resDBConnectinString = $"Data source={resBPath}; Version=3";

        private static string recordsDatabaseName = "records.s3db";
        static SqliteConnection recordsDBConn;
        private static string recordsBPath = Application.persistentDataPath + "/" + recordsDatabaseName;
        static string recordsDBConnectinString = $"Data source={recordsBPath}; Version=3";

        static DBHelper()
        {
            EnsureDBExistance();
        }

        private static void EnsureDBExistance()
        {
            using (SqliteConnection cnt = PUDBconn)
            {
                cnt.Open();
                using (SqliteCommand cmd = cnt.CreateCommand())
                {
                    cmd.CommandText = $"CREATE TABLE IF NOT EXISTS `{DBCnst.PlUpg.Self}` (`{DBCnst.PlUpg.PlayerName}`   TEXT NOT NULL UNIQUE,`{DBCnst.PlUpg.TrayTime}` INTEGER NOT NULL,`{DBCnst.PlUpg.MegaFruitMultiplyer}` INTEGER NOT NULL,`{DBCnst.PlUpg.TimeSlowTime}`    INTEGER NOT NULL,`{DBCnst.PlUpg.ShieldPerPowerup}`    INTEGER NOT NULL,`{DBCnst.PlUpg.HealMultiplyer}`   INTEGER NOT NULL,`{DBCnst.PlUpg.BombMultiplyer}`   INTEGER NOT NULL,PRIMARY KEY(`{DBCnst.PlUpg.PlayerName}`)); ";
                    cmd.ExecuteNonQuery();
                    try
                    {
                        cmd.CommandText = $"INSERT INTO `{DBCnst.PlUpg.Self}` (`{DBCnst.PlUpg.PlayerName}` ,`{DBCnst.PlUpg.TrayTime}` ,`{DBCnst.PlUpg.MegaFruitMultiplyer}` ,`{DBCnst.PlUpg.TimeSlowTime}`   ,`{DBCnst.PlUpg.ShieldPerPowerup}` ,`{DBCnst.PlUpg.HealMultiplyer}` ,`{DBCnst.PlUpg.BombMultiplyer}`) VALUES (@pl_name, @tr_t,@mfm,@tst,@spp,@he_m,@bo_m);";

                        cmd.Parameters.Add("@pl_name", System.Data.DbType.String).Value = "main_player";
                        cmd.Parameters.Add("@tr_t", System.Data.DbType.Int32).Value = 1;
                        cmd.Parameters.Add("@mfm", System.Data.DbType.Int32).Value = 1;
                        cmd.Parameters.Add("@tst", System.Data.DbType.Int32).Value = 1;
                        cmd.Parameters.Add("@spp", System.Data.DbType.Int32).Value = 1;
                        cmd.Parameters.Add("@he_m", System.Data.DbType.Int32).Value = 1;
                        cmd.Parameters.Add("@bo_m", System.Data.DbType.Int32).Value = 1;

                        cmd.ExecuteNonQuery();
                    }
                    catch { }


                    cmd.CommandText = $"CREATE TABLE IF NOT EXISTS `{DBCnst.PULvlVal.Self}` (`{DBCnst.PULvlVal.Level}` INTEGER NOT NULL UNIQUE,`{DBCnst.PULvlVal.TrayTime}` REAL NOT NULL,`{DBCnst.PULvlVal.MegaFruitMultiplyer}` REAL NOT NULL,`{DBCnst.PULvlVal.TimeSlowTime}`  REAL NOT NULL,`{DBCnst.PULvlVal.ShieldPerPowerup}`    INTEGER NOT NULL,`{DBCnst.PULvlVal.HealMultiplyer}`   INTEGER NOT NULL,`{DBCnst.PULvlVal.BombMultiplyer}`   INTEGER NOT NULL,PRIMARY KEY(`{DBCnst.PULvlVal.Level}`)); ";
                    cmd.ExecuteNonQuery();
                    try
                    {
                        cmd.CommandText = $"INSERT INTO `{DBCnst.PULvlVal.Self}` (`{DBCnst.PULvlVal.Level}` ,`{DBCnst.PULvlVal.TrayTime}` ,`{DBCnst.PULvlVal.MegaFruitMultiplyer}` ,`{DBCnst.PULvlVal.TimeSlowTime}`   ,`{DBCnst.PULvlVal.ShieldPerPowerup}` ,`{DBCnst.PULvlVal.HealMultiplyer}` ,`{DBCnst.PULvlVal.BombMultiplyer}`) VALUES (@lvl, @tr_t,@mfm,@tst,@spp,@he_m,@bo_m);";

                        cmd.Parameters.Add("@lvl", System.Data.DbType.Int32).Value = 1;
                        cmd.Parameters.Add("@tr_t", System.Data.DbType.Double).Value = 2;
                        cmd.Parameters.Add("@mfm", System.Data.DbType.Double).Value = 2;
                        cmd.Parameters.Add("@tst", System.Data.DbType.Double).Value = 3;
                        cmd.Parameters.Add("@spp", System.Data.DbType.Int32).Value = 1;
                        cmd.Parameters.Add("@he_m", System.Data.DbType.Int32).Value = 3;
                        cmd.Parameters.Add("@bo_m", System.Data.DbType.Int32).Value = 9;

                        cmd.ExecuteNonQuery();

                        cmd.CommandText = $"INSERT INTO `{DBCnst.PULvlVal.Self}` (`{DBCnst.PULvlVal.Level}` ,`{DBCnst.PULvlVal.TrayTime}` ,`{DBCnst.PULvlVal.MegaFruitMultiplyer}` ,`{DBCnst.PULvlVal.TimeSlowTime}`   ,`{DBCnst.PULvlVal.ShieldPerPowerup}` ,`{DBCnst.PULvlVal.HealMultiplyer}` ,`{DBCnst.PULvlVal.BombMultiplyer}`) VALUES (@lvl, @tr_t,@mfm,@tst,@spp,@he_m,@bo_m);";

                        cmd.Parameters.Add("@lvl", System.Data.DbType.Int32).Value = 2;
                        cmd.Parameters.Add("@tr_t", System.Data.DbType.Double).Value = 3;
                        cmd.Parameters.Add("@mfm", System.Data.DbType.Double).Value = 3;
                        cmd.Parameters.Add("@tst", System.Data.DbType.Double).Value = 4;
                        cmd.Parameters.Add("@spp", System.Data.DbType.Int32).Value = 2;
                        cmd.Parameters.Add("@he_m", System.Data.DbType.Int32).Value = 5;
                        cmd.Parameters.Add("@bo_m", System.Data.DbType.Int32).Value = 8;

                        cmd.ExecuteNonQuery();

                        cmd.CommandText = $"INSERT INTO `{DBCnst.PULvlVal.Self}` (`{DBCnst.PULvlVal.Level}` ,`{DBCnst.PULvlVal.TrayTime}` ,`{DBCnst.PULvlVal.MegaFruitMultiplyer}` ,`{DBCnst.PULvlVal.TimeSlowTime}`   ,`{DBCnst.PULvlVal.ShieldPerPowerup}` ,`{DBCnst.PULvlVal.HealMultiplyer}` ,`{DBCnst.PULvlVal.BombMultiplyer}`) VALUES (@lvl, @tr_t,@mfm,@tst,@spp,@he_m,@bo_m);";

                        cmd.Parameters.Add("@lvl", System.Data.DbType.Int32).Value = 3;
                        cmd.Parameters.Add("@tr_t", System.Data.DbType.Double).Value = 4;
                        cmd.Parameters.Add("@mfm", System.Data.DbType.Double).Value = 4;
                        cmd.Parameters.Add("@tst", System.Data.DbType.Double).Value = 5;
                        cmd.Parameters.Add("@spp", System.Data.DbType.Int32).Value = 3;
                        cmd.Parameters.Add("@he_m", System.Data.DbType.Int32).Value = 6;
                        cmd.Parameters.Add("@bo_m", System.Data.DbType.Int32).Value = 7;

                        cmd.ExecuteNonQuery();

                        cmd.CommandText = $"INSERT INTO `{DBCnst.PULvlVal.Self}` (`{DBCnst.PULvlVal.Level}` ,`{DBCnst.PULvlVal.TrayTime}` ,`{DBCnst.PULvlVal.MegaFruitMultiplyer}` ,`{DBCnst.PULvlVal.TimeSlowTime}`   ,`{DBCnst.PULvlVal.ShieldPerPowerup}` ,`{DBCnst.PULvlVal.HealMultiplyer}` ,`{DBCnst.PULvlVal.BombMultiplyer}`) VALUES (@lvl, @tr_t,@mfm,@tst,@spp,@he_m,@bo_m);";

                        cmd.Parameters.Add("@lvl", System.Data.DbType.Int32).Value = 4;
                        cmd.Parameters.Add("@tr_t", System.Data.DbType.Double).Value = 5;
                        cmd.Parameters.Add("@mfm", System.Data.DbType.Double).Value = 5;
                        cmd.Parameters.Add("@tst", System.Data.DbType.Double).Value = 6;
                        cmd.Parameters.Add("@spp", System.Data.DbType.Int32).Value = 4;
                        cmd.Parameters.Add("@he_m", System.Data.DbType.Int32).Value = 8;
                        cmd.Parameters.Add("@bo_m", System.Data.DbType.Int32).Value = 6;

                        cmd.ExecuteNonQuery();

                        cmd.CommandText = $"INSERT INTO `{DBCnst.PULvlVal.Self}` (`{DBCnst.PULvlVal.Level}` ,`{DBCnst.PULvlVal.TrayTime}` ,`{DBCnst.PULvlVal.MegaFruitMultiplyer}` ,`{DBCnst.PULvlVal.TimeSlowTime}`   ,`{DBCnst.PULvlVal.ShieldPerPowerup}` ,`{DBCnst.PULvlVal.HealMultiplyer}` ,`{DBCnst.PULvlVal.BombMultiplyer}`) VALUES (@lvl, @tr_t,@mfm,@tst,@spp,@he_m,@bo_m);";

                        cmd.Parameters.Add("@lvl", System.Data.DbType.Int32).Value = 5;
                        cmd.Parameters.Add("@tr_t", System.Data.DbType.Double).Value = 6;
                        cmd.Parameters.Add("@mfm", System.Data.DbType.Double).Value = 6;
                        cmd.Parameters.Add("@tst", System.Data.DbType.Double).Value = 7;
                        cmd.Parameters.Add("@spp", System.Data.DbType.Int32).Value = 5;
                        cmd.Parameters.Add("@he_m", System.Data.DbType.Int32).Value = 10;
                        cmd.Parameters.Add("@bo_m", System.Data.DbType.Int32).Value = 5;

                        cmd.ExecuteNonQuery();
                    }
                    catch { }


                    cmd.CommandText = $"CREATE TABLE IF NOT EXISTS `{DBCnst.PuUpgCost.Self}` (`{DBCnst.PuUpgCost.NxtLvlLvl}`   INTEGER NOT NULL UNIQUE,`{DBCnst.PuUpgCost.UpgCost}`  INTEGER NOT NULL,PRIMARY KEY(`{DBCnst.PuUpgCost.NxtLvlLvl}`)); ";
                    cmd.ExecuteNonQuery();
                    try
                    {
                        cmd.CommandText = $"INSERT INTO `{DBCnst.PuUpgCost.Self}` (`{DBCnst.PuUpgCost.NxtLvlLvl}` ,`{DBCnst.PuUpgCost.UpgCost}` ) VALUES (@nxt_lv_lv, @cost);";
                        cmd.Parameters.Add("@nxt_lv_lv", System.Data.DbType.Int32).Value = 2;
                        cmd.Parameters.Add("@cost", System.Data.DbType.Int32).Value = 100;
                        cmd.ExecuteNonQuery();

                        cmd.CommandText = $"INSERT INTO `{DBCnst.PuUpgCost.Self}` (`{DBCnst.PuUpgCost.NxtLvlLvl}` ,`{DBCnst.PuUpgCost.UpgCost}` ) VALUES (@nxt_lv_lv, @cost);";
                        cmd.Parameters.Add("@nxt_lv_lv", System.Data.DbType.Int32).Value = 3;
                        cmd.Parameters.Add("@cost", System.Data.DbType.Int32).Value = 500;
                        cmd.ExecuteNonQuery();

                        cmd.CommandText = $"INSERT INTO `{DBCnst.PuUpgCost.Self}` (`{DBCnst.PuUpgCost.NxtLvlLvl}` ,`{DBCnst.PuUpgCost.UpgCost}` ) VALUES (@nxt_lv_lv, @cost);";
                        cmd.Parameters.Add("@nxt_lv_lv", System.Data.DbType.Int32).Value = 4;
                        cmd.Parameters.Add("@cost", System.Data.DbType.Int32).Value = 2000;
                        cmd.ExecuteNonQuery();

                        cmd.CommandText = $"INSERT INTO `{DBCnst.PuUpgCost.Self}` (`{DBCnst.PuUpgCost.NxtLvlLvl}` ,`{DBCnst.PuUpgCost.UpgCost}` ) VALUES (@nxt_lv_lv, @cost);";
                        cmd.Parameters.Add("@nxt_lv_lv", System.Data.DbType.Int32).Value = 5;
                        cmd.Parameters.Add("@cost", System.Data.DbType.Int32).Value = 5000;
                        cmd.ExecuteNonQuery();
                    }
                    catch { }
                }
            }
            using (SqliteConnection cnt = ResDBconn)
            {
                cnt.Open();
                using (SqliteCommand cmd = cnt.CreateCommand())
                {
                    cmd.CommandText = $"CREATE TABLE IF NOT EXISTS `{DBCnst.FruitCur.Self}` (`{DBCnst.FruitCur.PlayerName}`   TEXT NOT NULL UNIQUE,`{DBCnst.FruitCur.FruitAmount}` INTEGER NOT NULL,PRIMARY KEY(`{DBCnst.FruitCur.PlayerName}`)); ";
                    cmd.ExecuteNonQuery();
                    try
                    {
                        cmd.CommandText = $"INSERT INTO `{DBCnst.FruitCur.Self}` (`{DBCnst.FruitCur.PlayerName}` ,`{DBCnst.FruitCur.FruitAmount}`) VALUES (@pl_name, @fr_am);";

                        cmd.Parameters.Add("@pl_name", System.Data.DbType.String).Value = "main_player";
                        cmd.Parameters.Add("@fr_am", System.Data.DbType.Int32).Value = 0;

                        cmd.ExecuteNonQuery();
                    }
                    catch { }

                    cmd.CommandText = $"CREATE TABLE IF NOT EXISTS `{DBCnst.PremiumCur.Self}` (`{DBCnst.PremiumCur.PlayerName}`   TEXT NOT NULL UNIQUE,`{DBCnst.PremiumCur.FruitAmount}` INTEGER NOT NULL,PRIMARY KEY(`{DBCnst.PremiumCur.PlayerName}`)); ";
                    cmd.ExecuteNonQuery();
                    try
                    {
                        cmd.CommandText = $"INSERT INTO `{DBCnst.PremiumCur.Self}` (`{DBCnst.PremiumCur.PlayerName}` ,`{DBCnst.PremiumCur.FruitAmount}`) VALUES (@pl_name, @fr_am);";

                        cmd.Parameters.Add("@pl_name", System.Data.DbType.String).Value = "main_player";
                        cmd.Parameters.Add("@fr_am", System.Data.DbType.Int32).Value = 0;

                        cmd.ExecuteNonQuery();
                    }
                    catch { }
                }
            }
            using (SqliteConnection cnt = RecordsDBconn)
            {
                cnt.Open();
                using (SqliteCommand cmd = cnt.CreateCommand())
                {
                    cmd.CommandText = $"CREATE TABLE IF NOT EXISTS `{DBCnst.BestScores.Self}` (`{DBCnst.BestScores.PlayerName}`   TEXT NOT NULL UNIQUE,`{DBCnst.BestScores.BestScore}` INTEGER NOT NULL,PRIMARY KEY(`{DBCnst.BestScores.PlayerName}`)); ";
                    cmd.ExecuteNonQuery();

                    try
                    {
                        cmd.CommandText = $"INSERT INTO `{DBCnst.BestScores.Self}` (`{DBCnst.BestScores.PlayerName}` ,`{DBCnst.BestScores.BestScore}`) VALUES (@pl_name, @score);";

                        cmd.Parameters.Add("@pl_name", System.Data.DbType.String).Value = "main_player";
                        cmd.Parameters.Add("@score", System.Data.DbType.Int32).Value = 0;

                        cmd.ExecuteNonQuery();
                    }
                    catch { }
                }
            }
        }

        public static SqliteConnection PUDBconn
        {
            get
            {
                try
                {
                    if (pUDBConn.Database == "main") { }
                }
                catch (Exception)
                {
                    pUDBConn = new SqliteConnection(pUDBConnectinString);
                }

                return pUDBConn;
            }
        }

        public static SqliteConnection ResDBconn
        {
            get
            {
                try
                {
                    if (resDBConn.Database == "main") { }
                }
                catch (Exception)
                {
                    resDBConn = new SqliteConnection(resDBConnectinString);
                }

                return resDBConn;
            }
        }

        public static SqliteConnection RecordsDBconn
        {
            get
            {
                try
                {
                    if (recordsDBConn.Database == "main") { }
                }
                catch (Exception)
                {
                    recordsDBConn = new SqliteConnection(recordsDBConnectinString);
                }

                return recordsDBConn;
            }
        }
    }
}
