﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Database
{
    public class DBPUVar
    {
        public object varVal;
        public int varLvl;
        public string plUpgsName;
        public string puLvlsValName;

        public DBPUVar(string PlUpgsName,string PuLvlsValName)
        {
            plUpgsName = PlUpgsName;
            puLvlsValName = PuLvlsValName;
        }
    }
}
