﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Mono.Data.Sqlite;
using System.Data;
using System.IO;
using UnityEngine.UI;
using UnityEngine;

namespace Assets.Database
{
    static class ClassesExtensions
    {
        public static T GetFieldValue<T>(this SqliteDataReader rdr,string index)
        {
            return rdr.GetFieldValue<T>(rdr.GetOrdinal(index));
        }

        public static int GetInt32(this SqliteDataReader rdr, string index)
        {
            return rdr.GetInt32(rdr.GetOrdinal(index));
        }

        public static string GetString(this SqliteDataReader rdr, string index)
        {
            return rdr.GetString(rdr.GetOrdinal(index));
        }

        public static float GetFloat(this SqliteDataReader rdr, string index)
        {
            return rdr.GetFloat(rdr.GetOrdinal(index));
        }
    }
}
